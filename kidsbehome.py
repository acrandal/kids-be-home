#!/usr/bin/env python
#
# Kids Be Home
#
# Uses hardware:
#   Raspberry PI (tested model 2)
#   Edimax USB Wifi card
#   Arcade buttons
#   Pull up resister board for GPIO pins
#   Case
#
# Designed to send SMS messages to a cell phone when buttons are pressed
# (1) Connect to GPIO pins
# (2) Wait on button presses
# (3) Send message via Twilio to SMS on cell phone
#
#   This work is licensed under a Creative Commons
#   Attribution-NonCommercial-ShareAlike 4.0
#   International License.
#
#   Copyright 2016
#   Aaron Crandall
#   acrandal@gmail.com
#



import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)

from time import sleep
import threading

from twilio.rest import TwilioRestClient
import smtplib

# To get Twilio account credentials, see: https://www.twilio.com/try-twilio
TWILIO_ACCOUNT_SID = #REDACTED
TWILIO_AUTH_TOKEN = #REDACTED
TO_PHONE = #REDACTED - the main "to" phone number
SECRET_PHONE = #REDACTED

# Button definition for BCM style GPIO numbering
butPin0 = 17
butPin1 = 18
butPin2 = 27
butPin3 = 22
butPin4 = 23
butPin5 = 24
butPin6 = 25

GPIO.setup(butPin0, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(butPin1, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(butPin2, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(butPin3, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(butPin4, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(butPin5, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(butPin6, GPIO.IN, pull_up_down=GPIO.PUD_UP)


#*****************************************************************#
# Send a given SMS to a registered account in Twilio
#  You cannot send to arbitrary phone numbers unless your Twilio
#  account is a paid account
#  Free accounts can only send to registered phone numbers
#
def send_sms(to, msg):
    global TWILIO_ACCOUNT_SID
    global TWILIO_AUTH_TOKEN
	#print("Sending SMS!")
	client = TwilioRestClient(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN)

	client.messages.create(
		#to="12083011666",
		to=to,
		from_="+14255288076",
		body=msg,
	)


#******************************************************************#
# Callback function for the change in state for any button
#
def button_change_state(channel):
    global TO_PHONE
    global SECRET_PHONE
	butNum = channel
	print "Any edge detected on button." + str(butNum)

	phoneto = TO_PHONE
	if GPIO.input(23):   # Yellow button is currently pressed
		phoneto = SECRET_PHONE   # Send to Jo's phone (lolz!)

	if butNum == 17:  # Green
		if GPIO.input(17):  # Button state is pressed
			send_sms(phoneto, "Friends are over.")
	if butNum == 18:  # White
		if GPIO.input(18):  # Button state is pressed
			send_sms(phoneto, "Please bring home pizza.")
	if butNum == 27:  # Blue
		if GPIO.input(27):  # Button state is pressed
			send_sms(phoneto, "Perry is home.")
	if butNum == 22:  # Red
		if GPIO.input(22):  # Button state is pressed
			send_sms(phoneto, "Kathryn is home.")
	if butNum == 23:  # Yellow
		if GPIO.input(23):  # Button state is pressed
			pass
			send_sms(TO_PHONE, "I'm going to Anna's House")


#********************************************************************************************#
# Register callbacks for all GPIO pins.
GPIO.add_event_detect(butPin0, GPIO.BOTH, callback=button_change_state, bouncetime=100)
GPIO.add_event_detect(butPin1, GPIO.BOTH, callback=button_change_state, bouncetime=100)
GPIO.add_event_detect(butPin2, GPIO.BOTH, callback=button_change_state, bouncetime=100)
GPIO.add_event_detect(butPin3, GPIO.BOTH, callback=button_change_state, bouncetime=100)
GPIO.add_event_detect(butPin4, GPIO.BOTH, callback=button_change_state, bouncetime=100)


print("Here we go! Press CTRL+C to exit")

try:
	while 1:        # Wait forever
		sleep(1)    # Sleep works just fine with the interrupts



except KeyboardInterrupt:
#	GPIO.cleanup()
	pass
GPIO.cleanup()


print "Done."





